export const required = function (value = '') {
  if (!value) return false
  return value.trim().length > 0
}

export const alpha = function (value) {
  if (!value) return false
  const regex = /^[a-zA-Z]*$/
  return regex.test(value)
}
export const numeric = function (value) {
  if (!value) return false
  const regex = /^[0-9]*$/
  return regex.test(value)
}
export const integer = function (value) {
  if (!value) return false
  const regex = /(^[0-9]*$)|(^-[0-9]+$)/
  return regex.test(value)
}

export const minLength = function (value = '', min = 1) {
  if (!value) return false
  return value.length >= min
}

export const maxLength = function (value = '', max = 99) {
  if (!value) return false
  return value.length <= max
}

export const minMaxLength = function (value = '', min = 1, max = 99) {
  if (!value) return false
  return value.length >= min && value.length <= max
}

export const exactLength = function (value = '', length = 1) {
  return minMaxLength(value, length, length)
}

export const email = function (value) {
  if (!value) return false
  const regex = /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/
  return regex.test(value)
}

export const phone = function (value) {
  if (!value) return false
  const regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/
  return regex.test(value)
}

export const url = function (value) {
  if (!value) return false
  const regex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i
  return regex.test(value)
}

export const sameAs = function (value, matcher) {
  if (!value) return false
  return value === matcher
}

export default {
  required,
  alpha,
  numeric,
  integer,
  minLength,
  email,
  phone,
  url,
  sameAs,
}