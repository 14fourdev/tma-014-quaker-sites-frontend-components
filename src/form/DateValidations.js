import dayjs from 'dayjs'

export const validDate = function (value) {
  return dayjs(value).isValid
}

export const minAgeDate = function (value, years) {
  let target = dayjs().subtract(years, 'year')
  let birthday = dayjs(value)
  return birthday.isBefore(target)
}
export const maxAgeDate = function (value, years) {
  let target = dayjs().add(years, 'year')
  let birthday = dayjs(value)
  return birthday.isAfter(target)
}

const createDate = (e, i) => i++ < 9 ? `0${i}` : `${i}`
const createYear = (e, i) => `${i + 1900}`
const newArray = (n, map) => Array.from(Array(n), map)

export const MONTHS = newArray(12, createDate)
export const DAYS = newArray(31, createDate)
export const YEARS = newArray(119, createYear).reverse()

export default {
  validDate,
  minAgeDate,
  maxAgeDate,
}
