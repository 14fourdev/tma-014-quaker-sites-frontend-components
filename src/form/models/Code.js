export const getModel = (fake = false) => {
  return {
    code: {
      value: fake ? '11111111111' : '',
      valid: null,
    },
    code02: {
      value: fake ? '11111111111' : '',
      valid: null,
    },
    retailer: {
      value: fake ? 'retailer': '',
      valid: null,
    },
  }
}

export const RETAILER_LIST = [
  `Albertson's`,
  `Dollar General`,
  `Food Lion`,
  `Giant`,
  `King Sooper's`,
  `Kroger`,
  `Martin's`,
  `Other`,
  `Publix`,
  `Ralph’s`,
  `Randall’s`,
  `Target`,
  `Wakefern`,
  `Walmart`,
]

export default {
  getModel,
  RETAILER_LIST,
}