export const getModel = (fake = false) => {
  return {
    email: {
      value: fake ? 'test@test.com' : '',
      valid: null,
    },
    firstName: {
      value: fake ? 'First' : '',
      valid: null,
    },
    lastName: {
      value: fake ? 'Last' : '',
      valid: null,
    },
    zip: {
      value: fake ? '12345' : '',
      valid: null,
    },
    month: {
      value: fake ? '01' : '',
      valid: null,
    },
    day: {
      value: fake ? '10' : '',
      valid: null,
    },
    year: {
      value: fake ? '2001' : '',
      valid: null,
    },
    rules: {
      value: fake,
      valid: null,
    },
  }
}

export default {
  getModel,
}