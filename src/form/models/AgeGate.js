export const getModel = (fake = false) => {
  return {
    month: {
      value: fake ? '01' : '',
      valid: null,
    },
    day: {
      value: fake ? '10' : '',
      valid: null,
    },
    year: {
      value: fake ? '2001' : '',
      valid: null,
    },
  }
}

export default {
  getModel,
}