import ModalBaseComponent from './ModalBase.vue'
import AgeGateFormComponent from './AgeGateForm.vue'
import CodeEntryFormComponent from './CodeEntryForm.vue'
import LoginFormComponent from './LoginForm.vue'
import RegisterFormComponent from './RegisterForm.vue'
import ImageSliderComponent from './ImageSlider.vue'

export const ModalBase = ModalBaseComponent
export const AgeGateForm = AgeGateFormComponent
export const CodeEntryForm = CodeEntryFormComponent
export const LoginForm = LoginFormComponent
export const RegisterForm = RegisterFormComponent
export const ImageSlider = ImageSliderComponent

export default {
  ModalBase,
  AgeGateForm,
  CodeEntryForm,
  LoginForm,
  RegisterForm,
  ImageSlider,
}